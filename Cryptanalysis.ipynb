{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Cryptanalysis of an SPN Toy Cipher Using Feedforward Neural Networks\n",
    "\n",
    "This notebook is a small wrap-up of experimental tests made during my master's thesis [Wab19], and contains the code and used hyperparameters for the experimental results presented in it. It demonstrates the attacks on block ciphers, presented in [AW04] and [Wab19], by using the SPN toy cipher of Heys' tutorial on linear and differential cryptanalysis [Hey02].\n",
    "\n",
    "In this demonstration, the SPN cipher is attacked by trying to revover the leftmost four bits of the last round key. For further information, see [Wab19]."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Required Imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Using TensorFlow backend.\n"
     ]
    }
   ],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from numpy import array\n",
    "from keras import backend as K\n",
    "from keras.models import Sequential \n",
    "from keras.layers import Dense\n",
    "from sklearn.metrics import mean_squared_error\n",
    "from sklearn.preprocessing import MinMaxScaler"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Helper Methods"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def int_to_bin(integer, digits):\n",
    "    \"\"\"\n",
    "    Converts the passed integer to binary representation and returns it in form of a list of length `digits`.\n",
    "    \"\"\"\n",
    "    form = '{0:0' + str(digits) + 'b}'\n",
    "    return [int(b) for b in form.format(integer)]\n",
    "    \n",
    "def bin_to_int(binary):\n",
    "    \"\"\"\n",
    "    Returns the integer which the passed binary list represents.\n",
    "    \"\"\"\n",
    "    return int(''.join(str(i) for i in binary), 2)\n",
    "\n",
    "def get_random_bit_lists(n, bits):\n",
    "    \"\"\"\n",
    "    Generates `n` distinct bit lists of length `bits`. \n",
    "    \"\"\"\n",
    "    different_random_values = np.random.choice(range(2**bits), n, replace=False)\n",
    "    lists = [list(array(list(np.binary_repr(value, width=bits))).astype(np.int)) for value in different_random_values]\n",
    "    return lists"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## SPN Toy Cipher"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "class BasicSPNCipher:\n",
    "    \n",
    "    sbox = [14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7]\n",
    "    sbox_rev = [14, 3, 4, 8, 1, 12, 10, 15, 7, 13, 9, 6, 11, 2, 0, 5]\n",
    "    \n",
    "    def __init__(self, keys=None):\n",
    "        self._keys = keys\n",
    "        \n",
    "    @staticmethod\n",
    "    def __perm(t):\n",
    "        return [t[0], t[4], t[8], t[12], t[1], t[5], t[9], t[13], t[2], t[6], t[10], t[14], t[3], t[7], t[11], t[15]]\n",
    "            \n",
    "    \n",
    "    @staticmethod\n",
    "    def __subst(t, reverse=False):\n",
    "        if reverse == False:\n",
    "            return int_to_bin(BasicSPNCipher.sbox[bin_to_int(t)], 4)\n",
    "        else:\n",
    "            return int_to_bin(BasicSPNCipher.sbox_rev[bin_to_int(t)], 4)\n",
    "        \n",
    "    @staticmethod\n",
    "    def __mix_key(t, k):\n",
    "        return [t[i] ^ k[i] for i in range(len(t))]\n",
    "    \n",
    "    @staticmethod\n",
    "    def __subst_step(t, reverse=False):\n",
    "        output = []\n",
    "        inputs = [t[i:i+4] for i in range(0, 16, 4)]\n",
    "        for inp in inputs:\n",
    "            output += BasicSPNCipher.__subst(inp, reverse)\n",
    "        return output\n",
    "        \n",
    "    def encrypt(self, p, rounds=4):      \n",
    "        for i in range(3):\n",
    "            p = BasicSPNCipher.__mix_key(p, self._keys[i])\n",
    "            p = BasicSPNCipher.__subst_step(p)\n",
    "            p = BasicSPNCipher.__perm(p)\n",
    "        p = BasicSPNCipher.__mix_key(p, self._keys[3])\n",
    "        if rounds == 3:\n",
    "            return p\n",
    "        p = BasicSPNCipher.__subst_step(p)\n",
    "        return BasicSPNCipher.__mix_key(p, self._keys[4])\n",
    "    \n",
    "    def decrypt(self, c):\n",
    "        c = BasicSPNCipher.__mix_key(c, self._keys[4])\n",
    "        c = BasicSPNCipher.__subst_step(c, reverse=True)\n",
    "        c = BasicSPNCipher.__mix_key(c, self._keys[3])\n",
    "        for i in [2, 1, 0]:\n",
    "            c = BasicSPNCipher.__perm(c)\n",
    "            c = BasicSPNCipher.__subst_step(c, reverse=True)\n",
    "            c = BasicSPNCipher.__mix_key(c, self._keys[i])\n",
    "        return c\n",
    "    \n",
    "    def partial_decrypt(self, c, key):\n",
    "        c = BasicSPNCipher.__mix_key(c, key)\n",
    "        return BasicSPNCipher.__subst_step(c, reverse=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model and Training Hyperparameters\n",
    "This model and hyperparameters are used for attacking the cipher with the subsequent methods. Note that due to the cipher and implementation, the input layer should have 16 and the output layer 4 units."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "EPOCHS = 200\n",
    "BATCH_SIZE = 128\n",
    "\n",
    "def get_model():\n",
    "    model = Sequential()\n",
    "    model.add(Dense(256, input_dim=16, activation='relu'))\n",
    "    model.add(Dense(4, activation='sigmoid'))\n",
    "    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['mse'])\n",
    "    return model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Attack One\n",
    "This is the attack presented by Albassal and Wahdan in [AW04]. Feel free to adjust the number of known plaintext-ciphertext pairs and number of tests. Each test tries to recover the leftmost four bits of the last round key. How many of the known pairs are used for training the neural network is also adjustable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Recovered the correct key:                  7/10\n",
      "Correct key was among the top 3 candidates: 10/10\n"
     ]
    }
   ],
   "source": [
    "NUM_OF_PAIRS = 6000\n",
    "NUM_OF_TESTS = 10\n",
    "# Fraction to use for the training set. The rest is used for evaluating the network.\n",
    "FRACTION = 0.7\n",
    "\n",
    "top_1 = 0\n",
    "top_3 = 0\n",
    "for t in range(NUM_OF_TESTS):\n",
    "    key = get_random_bit_lists(5, 16)\n",
    "    correct_partial_subkey = key[4][:4]\n",
    "    cipher = BasicSPNCipher(key)\n",
    "    plaintexts = get_random_bit_lists(NUM_OF_PAIRS, 16)\n",
    "    ciphertexts = [cipher.encrypt(p) for p in plaintexts]\n",
    "\n",
    "    training_ex_num = int(FRACTION * NUM_OF_PAIRS)\n",
    "    p_training = plaintexts[:training_ex_num]\n",
    "    p_test = plaintexts[training_ex_num:]\n",
    "\n",
    "    results = []\n",
    "    for k in range(16):\n",
    "        partial_subkey = int_to_bin(k, 4)\n",
    "        subkey = partial_subkey + 12*[0]\n",
    "        c_tilde = [cipher.partial_decrypt(c, subkey)[:4] for c in ciphertexts]\n",
    "        c_tilde_training = c_tilde[:training_ex_num]\n",
    "        c_tilde_test = c_tilde[training_ex_num:]\n",
    "        model = get_model()\n",
    "        model.fit(array(p_training), array(c_tilde_training),\n",
    "                  epochs=EPOCHS, batch_size=BATCH_SIZE,\n",
    "                  verbose=0)\n",
    "        results.append(model.evaluate(array(p_test), array(c_tilde_test), verbose=0)[1])\n",
    "        K.clear_session()\n",
    "\n",
    "    results = array(results)\n",
    "    correct_key = bin_to_int(correct_partial_subkey)\n",
    "    if correct_key in results.argsort()[:3]:\n",
    "        top_3 += 1\n",
    "        if correct_key in results.argsort()[:1]:\n",
    "            top_1 += 1\n",
    "    \n",
    "print(\"Recovered the correct key:                  \" + str(top_1) + \"/\" + str(NUM_OF_TESTS)) \n",
    "print(\"Correct key was among the top 3 candidates: \" + str(top_3) + \"/\" + str(NUM_OF_TESTS))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Attack Two\n",
    "This is Method 1 of [Wab19], which is basically the attack by Albassal and Wahdan, but without splitting the set of plaintext-ciphertext pairs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Recovered the correct key:                  9/10\n",
      "Correct key was among the top 3 candidates: 9/10\n"
     ]
    }
   ],
   "source": [
    "NUM_OF_PAIRS = 6000\n",
    "NUM_OF_TESTS = 10\n",
    "\n",
    "top_1 = 0\n",
    "top_3 = 0\n",
    "for t in range(NUM_OF_TESTS):\n",
    "    key = get_random_bit_lists(5, 16)\n",
    "    correct_partial_subkey = key[4][:4]\n",
    "    cipher = BasicSPNCipher(key)\n",
    "    plaintexts = get_random_bit_lists(NUM_OF_PAIRS, 16)\n",
    "    ciphertexts = [cipher.encrypt(p) for p in plaintexts]\n",
    "\n",
    "    results = []\n",
    "    for k in range(16):\n",
    "        partial_subkey = int_to_bin(k, 4)\n",
    "        subkey = partial_subkey + 12*[0]\n",
    "        c_tilde = [cipher.partial_decrypt(c, subkey)[:4] for c in ciphertexts]\n",
    "        model = get_model()\n",
    "        model.fit(array(plaintexts), array(c_tilde),\n",
    "                  epochs=EPOCHS, batch_size=BATCH_SIZE,\n",
    "                  verbose=0)\n",
    "        results.append(model.evaluate(array(plaintexts), array(c_tilde), verbose=0)[1])\n",
    "        K.clear_session()\n",
    "    results = array(results)\n",
    "\n",
    "    correct_key = bin_to_int(correct_partial_subkey)\n",
    "    if correct_key in results.argsort()[:3]:\n",
    "        top_3 += 1\n",
    "        if correct_key in results.argsort()[:1]:\n",
    "            top_1 += 1\n",
    "\n",
    "print(\"Recovered the correct key:                  \" + str(top_1) + \"/\" + str(NUM_OF_TESTS)) \n",
    "print(\"Correct key was among the top 3 candidates: \" + str(top_3) + \"/\" + str(NUM_OF_TESTS))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Attack Three\n",
    "These are different versions of Method 2 of [Wab19], which are outlined in Section 4.4.1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Method 2\n",
      "Recovered the correct key:                  5/10\n",
      "Correct key was among the top 3 candidates: 9/10\n",
      "\n",
      "Method 2 (Average)\n",
      "Recovered the correct key:                  6/10\n",
      "Correct key was among the top 3 candidates: 9/10\n",
      "\n",
      "Normalization (Feature Scaling)\n",
      "Recovered the correct key:                  0/10\n",
      "Correct key was among the top 3 candidates: 4/10\n",
      "\n",
      "Normalization (Same Average)\n",
      "Recovered the correct key:                  2/10\n",
      "Correct key was among the top 3 candidates: 5/10\n"
     ]
    }
   ],
   "source": [
    "NUM_OF_PAIRS = 6000\n",
    "NUM_OF_TESTS = 10\n",
    "# Number of NNs used for retrieving reference values.\n",
    "NUM_OF_NN_REF = 16\n",
    "# Number of NNs  used for each possible partial last round key.\n",
    "NUM_OF_NN = 1\n",
    "\n",
    "average_mse_top_1 = 0\n",
    "average_mse_top_3 = 0\n",
    "\n",
    "norm_sum_top_1 = 0\n",
    "norm_sum_top_3 = 0\n",
    "\n",
    "norm_feature_scaling_top_1 = 0\n",
    "norm_feature_scaling_top_3 = 0\n",
    "\n",
    "mses_top_1 = 0\n",
    "mses_top_3 = 0\n",
    "\n",
    "for t in range(NUM_OF_TESTS):\n",
    "    key = get_random_bit_lists(5, 16)\n",
    "    correct_partial_subkey = key[4][:4]\n",
    "    cipher = BasicSPNCipher(key)\n",
    "    plaintexts = get_random_bit_lists(NUM_OF_PAIRS, 16)\n",
    "    ciphertexts = [cipher.encrypt(p) for p in plaintexts]\n",
    "    \n",
    "    ref_average_mse = []\n",
    "    ref_mses = []\n",
    "    for i in range(NUM_OF_NN_REF):\n",
    "        training_key = get_random_bit_lists(5, 16)\n",
    "        training_cipher = BasicSPNCipher(training_key)\n",
    "        ref_ciphertexts = array([training_cipher.encrypt(p, rounds=3)[:4] for p in plaintexts])\n",
    "        plaintexts = array(plaintexts)\n",
    "        train_model = get_model()\n",
    "        train_model.fit(plaintexts, ref_ciphertexts,\n",
    "                        epochs=EPOCHS, batch_size=BATCH_SIZE,\n",
    "                        verbose=0)\n",
    "        ref_preds = train_model.predict(plaintexts)\n",
    "        ref_mses_i = mean_squared_error(ref_ciphertexts, ref_preds, multioutput='raw_values')\n",
    "        ref_mses.append(ref_mses_i)\n",
    "        ref_average_mse.append(np.average(ref_mses_i))\n",
    "        K.clear_session()\n",
    "    ref_mses = np.average(ref_mses, axis=0)\n",
    "    ref_average_mse = np.average(ref_average_mse)\n",
    "          \n",
    "    all_average_mse = []\n",
    "    all_mses = []\n",
    "    for k in range(16):\n",
    "        partial_subkey = int_to_bin(k, 4)\n",
    "        subkey = partial_subkey + 12*[0]\n",
    "        c_tilde = array([cipher.partial_decrypt(c, subkey)[:4] for c in ciphertexts])\n",
    "        mses = []\n",
    "        average_mse = []\n",
    "        for i in range(NUM_OF_NN):\n",
    "            model= get_model()\n",
    "            model.fit(plaintexts, c_tilde,\n",
    "                      epochs=EPOCHS, batch_size=BATCH_SIZE,\n",
    "                      verbose=0)\n",
    "            preds = model.predict(plaintexts)\n",
    "            mses_i = mean_squared_error(c_tilde, preds, multioutput='raw_values')\n",
    "            mses.append(mses_i)\n",
    "            average_mse.append(np.average(mses_i))\n",
    "            K.clear_session()\n",
    "        all_mses.append(np.average(array(mses), axis=0).tolist())\n",
    "        all_average_mse.append(np.average(array(average_mse)))\n",
    "\n",
    "    m2_differences = []\n",
    "    m2_average_differences = []\n",
    "    norm_feature_scaling_differences = []\n",
    "    norm_sum_differences = []\n",
    "\n",
    "    for mses in all_mses:\n",
    "        m2_differences.append(np.sum(np.square(mses - ref_mses)))\n",
    "        \n",
    "    for average_mse in all_average_mse:\n",
    "        m2_average_differences.append(np.sum(np.abs(average_mse - ref_average_mse)))\n",
    "        \n",
    "    for mses in all_mses:\n",
    "        norm_mses = mses / np.sum(mses)\n",
    "        norm_ref_mses = ref_mses / np.sum(ref_mses)\n",
    "        norm_sum_differences.append(np.sum(np.square(norm_mses - norm_ref_mses)))\n",
    "        \n",
    "    for mses in all_mses:\n",
    "        scaler = MinMaxScaler()\n",
    "        mses = np.array(mses).reshape(-1,1)\n",
    "        scaler.fit(mses)\n",
    "        mses = scaler.transform(mses)\n",
    "        ref_mses = ref_mses.reshape(-1,1)\n",
    "        scaler.fit(ref_mses)\n",
    "        ref_mses = scaler.transform(ref_mses)\n",
    "        norm_feature_scaling_differences.append(np.sum(np.square(mses - ref_mses)))\n",
    "    \n",
    "    m2_differences = array(m2_differences)\n",
    "    m2_average_differences = array(m2_average_differences)\n",
    "    norm_feature_scaling_differences = array(norm_feature_scaling_differences)\n",
    "    norm_sum_differences = array(norm_sum_differences)\n",
    "\n",
    "    correct_key = bin_to_int(correct_partial_subkey)\n",
    "            \n",
    "    if correct_key in m2_differences.argsort()[:3]:\n",
    "        mses_top_3 += 1\n",
    "        if correct_key in m2_differences.argsort()[:1]:\n",
    "            mses_top_1 += 1\n",
    "            \n",
    "    if correct_key in m2_average_differences.argsort()[:3]:\n",
    "        average_mse_top_3 += 1\n",
    "        if correct_key in m2_average_differences.argsort()[:1]:\n",
    "            average_mse_top_1 += 1\n",
    "            \n",
    "    if correct_key in norm_sum_differences.argsort()[:3]:\n",
    "        norm_sum_top_3 += 1\n",
    "        if correct_key in norm_sum_differences.argsort()[:1]:\n",
    "            norm_sum_top_1 += 1\n",
    "            \n",
    "    if correct_key in norm_feature_scaling_differences.argsort()[:3]:\n",
    "        norm_feature_scaling_top_3 += 1\n",
    "        if correct_key in norm_feature_scaling_differences.argsort()[:1]:\n",
    "            norm_feature_scaling_top_1 += 1\n",
    "            \n",
    "print(\"Method 2\")\n",
    "print(\"Recovered the correct key:                  \" + str(mses_top_1) + \"/\" + str(NUM_OF_TESTS)) \n",
    "print(\"Correct key was among the top 3 candidates: \" + str(mses_top_3) + \"/\" + str(NUM_OF_TESTS))\n",
    "print()\n",
    "\n",
    "print(\"Method 2 (Average)\")\n",
    "print(\"Recovered the correct key:                  \" + str(average_mse_top_1) + \"/\" + str(NUM_OF_TESTS)) \n",
    "print(\"Correct key was among the top 3 candidates: \" + str(average_mse_top_3) + \"/\" + str(NUM_OF_TESTS))\n",
    "print()\n",
    "\n",
    "print(\"Normalization (Feature Scaling)\")\n",
    "print(\"Recovered the correct key:                  \" + str(norm_feature_scaling_top_1) + \"/\" + str(NUM_OF_TESTS)) \n",
    "print(\"Correct key was among the top 3 candidates: \" + str(norm_feature_scaling_top_3) + \"/\" + str(NUM_OF_TESTS))\n",
    "print()\n",
    "\n",
    "print(\"Normalization (Same Average)\")\n",
    "print(\"Recovered the correct key:                  \" + str(norm_sum_top_1) + \"/\" + str(NUM_OF_TESTS)) \n",
    "print(\"Correct key was among the top 3 candidates: \" + str(norm_sum_top_3) + \"/\" + str(NUM_OF_TESTS))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See [Wab19, p. 51] for more reliable results."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Position-Specific Performances\n",
    "The results above indicate that the performances for the individual bit positions differ. This is visualized for the encryption function of the reduced cipher (3 rounds):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAZIAAAEWCAYAAABMoxE0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMi4zLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvIxREBQAAIABJREFUeJzt3X+cXFV9//HXmyUhWAQSEn9AMEGJdZOIQdfoV6MYBA1VA+2XChEU2lW+2hKrqAVcKxBdq6BFpbRKG8RfWX5VawQjUtwgKaAsECJhRdKIsAQhmFB+hyR8vn/cszAZZndm987NzO6+n4/HPHbmnnPPnHtn9n7uPefOOYoIzMzMhmuXRlfAzMxGNgcSMzPLxYHEzMxycSAxM7NcHEjMzCwXBxIzM8vFgWQMkfR5SQ9J+kOj69Jokv5U0q2SHpX00QbXZbqkkLRrer1C0gk1rPcWSXcOkn6RpM/Xu37DWP8bkv4hbz3ykLRW0tsGSa9pn1tl8u9Impeku4EXA9uBx4GfAIsj4rFhlLU/8FtgWkQ8WM96jkSSlgKPRMTH61TemcCBEXH8MNadDvwOGBcR2+pRn1TuRUBfRHwmZznTGaR+Zd/TrcD1wIcj4t4Ked8GfC8iplap9/uAp9PjZrLv/W/ybEdJ+WcyzM/KKvMVSfN7T0TsAbwWeD0w5INCOpOcBvxxOEFkuGeiTW4asHY4K47S/ZFX//f0pcADwHk5yzs7lTcVeBC4KGd5ViAHkhEiIu4DVgCzASTtJWmppPsl3ZearVpS2omS/lvSuZI2ASuBq4F9JT2WzviQtDBd8j8saaWk1v73k3S3pFMlrQEel7RrWvYpSWskPZ7e/8WpWeBRSf8laWJJGZdJ+oOk/5X0C0mzStIuknS+pCvTur+U9IqS9FmSrpa0SdIDkj6dlu8i6TRJ/yPpj5IulTQppU2Q9L20/GFJN0l6cfm+lPRzYD7wz2l/vDLtz+9I2ijp95I+I2mXAfbnmdU+r9QU9GFJd0nanLZVKa1F0pdTM+N64F1l666U9EFJu6XtmF2SNkXSk5JeJOltkvpK0g6WdEvan5cAE0rSTpS0qkIdD0zP36Wsqe8RSfems/Yhi4ingMuBmSXvc1H6fv4J2Xe4/3v4mKR9q5T3BLCM5773u0n6qqQN6fFVSbultMmSrkj7bJOk60o+w7slHSZpAfBp4Jj0/rel9JWSPpie75I+/99LejB9L/ZKaf3NfCdIuid9hh0l2zpXUk/ajw9I+qfh7MeRxoFkhFDWNPVnwK1p0beBbcCBwMHAO4APlqzyBmA98CLgcOAIYENE7BERJ0p6JdAFfAyYQtZs9mNJ40vKWER2kNu7pEnj/6byXgm8h+zA8GlgMtn3qbS/YQUwI9XhFuD7ZZu1CDgLmAisAzrTtr4Q+C/gp8C+aRuvSet8FDgKOCSlbQbOT2knAHsB+wP7AB8GnizflxFxKHAdcHLaH78lO4PeC3h5KvsDwF8NsD87y8scwLvJriJfA7wXeGda/qGUdjDQBhxdaeWI2AL8gGw/9XsvcG35lWX63P4T+C4wCbiM7LOq1eNk27w32Wf+EUlHDWH9/nq8ADgGuLE8LSIeZ8fv4R4RsaFKeXsAx/Hc974DeCMwh2y/zuW5q/RPAH1k3+cXk30vd2i7j4ifAl8ALknv/5oKb3tieswn+z7sAfxzWZ55wJ8Cbwc+q+dOwr4GfC0i9gReAVw62PaNFg4kze8/JT0MrAKuBb6QzrKPAD4WEY+ng8q5wLEl622IiPMiYltEPO9gSvbPfmVEXB0RW4EvA7sDbyrJ8/WIuLds/fMi4oF0hXQd8MuIuDUd9H5IdnAEICIujIhHU9qZwGv6z+ySH0TEr1KQ+j7ZwQGyg+wfIuIrEfFUKuOXKe3/AR0R0VdS7tHKmpu2kgWQAyNie0TcHBGPVNvByq7kjgFOT+91N/AV4P1D2J+VfDEiHo6Ie4Duku17L/DVtG83Af84SBnL2DGQvC8tK/dGYFwqd2tEXA7cVGM9iYiVEfHriHgmItaQnWQcUuv6PPc9fYTsROOcIaxbySdTeevIDuQnpuXHAUsi4sGI2Eh2ItL/OW0la1qblvbBdTG8TuDjgH+KiPWpP/J04Fjt2KR5VkQ8GRG3AbeRBbX+OhwoaXJEPBYRzwuoo5EDSfM7KiL2johpEfE36SA2jeygcX+6jH8Y+CbZ2XK/53V0ltkX+H3/i4h4Jq2zX5UyHih5/mSF13vAs803X0xNUI8Ad6c8k0vyl9499kT/umRXFP8zQL2nAT8s2e5esk7eF5OdjV8FXJyaPc6WNG6AckpNBsZTsj/S82r7opqBtm/fsvJK37fcz4HdJb1B0jSyYPTDCvn2Be4rO3AOVu4OUvndqWnvf8mu5iZXW6/EURGxN7AbcDJwraSXDGH9cl9O3/uXRMTCiOj/PuzwvU3P+5vHziELPD+TtF7SacN870rvsSvZd6zfQJ9tO9nV+m+UNa2+e5h1GFEcSEame4EtwOT0z7Z3ROwZEbNK8lQ7E9tAdlAGILXf7w/cN4QyBvM+4EjgMLImo+n9b1XDuveSNQsMlHZEyXbvHRETIuK+dBZ6VkTMJLuyejdZc001D5GdSU4rWfYy6rcvyt1Ptq9L36uiFOAvJbsqeR9wRUQ8OkCZ+/X3w1Qo93HgBf0vKhzklwHLgf0jYi/gG9T2WZXXd3tE/IAsuM+rlGWoZZbZ4XtLto0b0ns/GhGfiIiXkzW7niLp7cOoQ6X32MaOJ00VRcRdEbGI7KTuS8DlqW9oVHMgGYEi4n7gZ8BXJO2ZOgdfIWkoTRGXAu+S9PZ01v4JsuB0fZ2q+cJU3h/JDmBfGMK6VwAvkfSx1Ln6QklvSGnfADrT2Xl/5/OR6fl8Sa9OTVWPkAWH7dXeLCK2k+2PzvRe04BTgO8Noc5DcSnwUUlTld2cUO3MeRlZ09txVG7WAriB7GD3UWU3RvwFWf9Bv9uAWZLmSJrA828YeCGwKSKekjSXLGgNmTJHkvV79VbI8gCwT1kT51B0AZ9Jn/tk4LOkz0nSuyUdmILpI2SffaXP/wFgen9H/ADv8XFJB6Q+mv4+laq3Zks6XtKUdALwcFpc9Ts40jmQjFwfIGuOuYOsw/lysvbhmkTEncDxZJ3MD5Gdwb0nIp6uU/2+Q9YkcF+qY81txemM+/BUpz8Ad5F1fELWmbmcrPni0VRuf5B5Cdl+eITsIHYttQeDxWRn7evJ+qOWARfWWuch+jeyJrjbyG5C+MFgmVP/0ONkTS4rBsjzNPAXZH0Jm8kCzw9K0n8LLCG7ieEusm0s9TfAkrRPP8vQO4l/LOkxsn3fCZwQEc+7vTr9FqQLWJ+aJwe9a6uCzwM9wBrg12T7r/9HlzPItu8xssD6LxGxskIZl6W/f5R0S4X0C8maSX9B9vuZp8i+H7VYAKxN++JrwLHpTrZRzT9INDOzXHxFYmZmuTiQmJlZLg4kZmaWiwOJmZnlMiYGn5s8eXJMnz690dUwMxtRbr755ociYkq1fGMikEyfPp2enp5GV8PMbESRVNPoCG7aMjOzXBxIzMwsl0IDiaQFku6UtK7SAGqSTpF0h7L5La4pGfZijqQblM2VsUbSMSXrHKBs7oq7JF1SNuy5mZntZIUFkjTe0flkw53PBBZJmlmW7VagLSIOIhva4uy0/AngA2kQwgXAVyXtndK+BJwbETPIhoJoL2obzMysuiKvSOYC69KY/k8DF5ONBvusiOiObAY0yMZMmpqW/zYi7krPN5BNtTklDcZ2KFnQgWxypyFPvmNmZvVTZCDZjx3nXOhjx/kdyrVTYUC6NBLpeLL5KfYBHi4ZhXPAMiWdpGzKy56NGzcOo/o2EnV1dTF79mxaWlqYPXs2XV1dja6S2ahX5O2/leYyqDhCpKTjyaYcPaRs+UvJRuE8ISKeKZtrYdAyI+IC4AKAtrY2j0w5BnR1ddHR0cHSpUuZN28eq1ator09a/lctGhRlbXNbLiKvCLpY8fJe6aSJqApJekwsnmYF6apU/uX7wlcCXymZLrKh4C9S6a8rFimjU2dnZ0sXbqU+fPnM27cOObPn8/SpUvp7Kx1inUzG44iA8lNwIx0l9V4svnEl5dmkHQw2RSxC9O84/3Lx5NNJ/qdiOifO4A0jWg3cHRadALwowK3wUaQ3t5e5s3bcVK+efPm0dtbaX4lM6uXwgJJ6sc4mWwCn17g0ohYK2mJpIUp2zlkcx1fJmm1pP5A817grcCJaflqSXNS2qlkU2iuI+szWVrUNtjI0trayqpVO87XtGrVKlpbWxtUI7OxodAhUiLiJ8BPypZ9tuT5YQOs9z0GmNkuItaz4xSiZgB0dHTQ3t7+vD4SN22ZFWtMjLVlY0N/h/rixYvp7e2ltbWVzs5Od7SbFWxMTLXb1tYWHrTRzGxoJN0cEW3V8nmsLTMzy8WBxMzMcnEgMTOzXBxIzMwsFwcSMzPLxYHEzMxycSAxM7NcHEjMzCwXBxIzM8vFgcTMzHJxIDEzs1wcSMzMLBcHEjMzy8WBxMzMcnEgMTOzXBxIzMwsFwcSMzPLxYHEzMxyKTSQSFog6U5J6ySdViH9FEl3SFoj6RpJ00rSfirpYUlXlK1zkaTfSVqdHnOK3AYzMxtcYYFEUgtwPnAEMBNYJGlmWbZbgbaIOAi4HDi7JO0c4P0DFP+piJiTHqvrXHUzMxuCIq9I5gLrImJ9RDwNXAwcWZohIroj4on08kZgaknaNcCjBdbPzMzqoMhAsh9wb8nrvrRsIO3AihrL7kzNYedK2q1SBkknSeqR1LNx48YaizUzs6EqMpCowrKomFE6Hmgja86q5nTgVcDrgUnAqZUyRcQFEdEWEW1TpkyprcZmZjZkRQaSPmD/ktdTgQ3lmSQdBnQACyNiS7VCI+L+yGwBvkXWhGZmZg1SZCC5CZgh6QBJ44FjgeWlGSQdDHyTLIg8WEuhkl6a/go4Cri9rrU2M7Mh2bWogiNim6STgauAFuDCiFgraQnQExHLyZqy9gAuy+IC90TEQgBJ15E1Ye0hqQ9oj4irgO9LmkLWdLYa+HBR22BmZtUpomK3xajS1tYWPT09ja6GmdmIIunmiGirls+/bDczs1wcSMzMLBcHEjMzy8WBxMzMcinsri2zoqQ7/OpmLNxwYlYkBxIbcWo58EtygDDbSdy0ZWZmuTiQmJlZLm7aMjOrk7Haf+crEjMbUFdXF7Nnz6alpYXZs2fT1dXV6Co1tYio6VFr3pHCVyRmVlFXVxcdHR0sXbqUefPmsWrVKtrb2wFYtGhRg2tnzcRXJGZWUWdnJ0uXLmX+/PmMGzeO+fPns3TpUjo7OxtdNWsyHrTRRiXf/ptfS0sLTz31FOPGjXt22datW5kwYQLbt29vYM1GvpHy/fSgjWaWS2trK6tWrdph2apVq2htbW1QjaxZOZCYWUUdHR20t7fT3d3N1q1b6e7upr29nY6OjkZXzZqMO9vNrKL+DvXFixfT29tLa2srnZ2d7mi353EfiY1KI6UNuhnU87cP3ue1GSnfz1r7SHxFYjbGeewyy8t9JGZmlosDiZmZ5VJoIJG0QNKdktZJOq1C+imS7pC0RtI1kqaVpP1U0sOSrihb5wBJv5R0l6RLJI0vchvMzGxwhQUSSS3A+cARwExgkaSZZdluBdoi4iDgcuDskrRzgPdXKPpLwLkRMQPYDLTXu+5mZla7Iq9I5gLrImJ9RDwNXAwcWZohIroj4on08kZgaknaNcCjpfmV3V5yKFnQAfg2cFQx1Tcze86kSZOQVJcHULeyJk2a1OA9U+xdW/sB95a87gPeMEj+dmBFlTL3AR6OiG0lZe5XKaOkk4CTAF72spfVUl9rApMmTWLz5s11Katet7VOnDiRTZs21aUsG7k2b97clHeu1Xvo+uEoMpBU2rqKn4Kk44E24JB6lRkRFwAXQPY7kirlWpNoxn/WZvhHNWtmRQaSPmD/ktdTgQ3lmSQdBnQAh0TEliplPgTsLWnXdFVSsUwzM9t5iuwjuQmYke6yGg8cCywvzSDpYOCbwMKIeLBagZGdqnYDR6dFJwA/qmutzcxsSAoLJOmK4WTgKqAXuDQi1kpaImlhynYOsAdwmaTVkp4NNJKuAy4D3i6pT9I7U9KpwCmS1pH1mSwtahvMzKw6j7VlTaUZh+JoxjrtbN4HzbsPiqyX5yMxM7OdwoHEzMxy8ei/ZmY1iDP2hDP3anQ1nifO2LPRVXAgsebSjP+szfCPao2nsx5p3j6SMxtbBwcSayrN+M/aDP+oZs3MfSRmZpaLA4mZmeXiQGJmZrk4kJiZWS4OJGZmlosDiZmZ5eLbf63pNNv8HxMnTmx0FcyamgOJNZV6/YakWQfYMxuNHEjMzGrUbFfL0BxXzA4kZqPUpEmT2Lx5c93Kq9dBdOLEiWzatKkuZe1M9bzCHW1XzA4kZqPU5s2bm/Jg1Yxn9ZaP79oyM7NcHEjMzCwXBxIzM8vFgcTMzHIpNJBIWiDpTknrJJ1WIf0USXdIWiPpGknTStJOkHRXepxQsnxlKnN1eryoyG0wM7PBFXbXlqQW4HzgcKAPuEnS8oi4oyTbrUBbRDwh6SPA2cAxkiYBZwBtQAA3p3X772U8LiJ6iqq72WjQjLNNgmecHI2KvP13LrAuItYDSLoYOBJ4NpBERHdJ/huB49PzdwJXR8SmtO7VwAKgq8D6mo0qzTjbJHjGydGoyKat/YB7S173pWUDaQdW1Ljut1Kz1j9ogJvSJZ0kqUdSz8aNG4deezMzq0mRgaTSAb7i6ZGk48masc6pYd3jIuLVwFvS4/2VyoyICyKiLSLapkyZMqSKm5lZ7YoMJH3A/iWvpwIbyjNJOgzoABZGxJZq60bEfenvo8AysiY0MzNrkCIDyU3ADEkHSBoPHAssL80g6WDgm2RB5MGSpKuAd0iaKGki8A7gKkm7Spqc1h0HvBu4vcBtMDOzKgrrbI+IbZJOJgsKLcCFEbFW0hKgJyKWkzVl7QFclro67omIhRGxSdLnyIIRwJK07E/IAsq4VOZ/Af9W1DaYmVl1asa7Ouqtra0tenp8t/BYMtpGVx2OZt0HzVqvnWmk7ANJN0dEW7V8/mW7mZnlMmggSXdT9T9/c1nayUVVyszMRo5qVySnlDw/ryztr+tcFzMzG4GqdbZrgOeVXptZk2nGSaSaYWrYogxlf9eSdyT0o0D1QBIDPK/02syaiKeG3fnG6j6qFkheJWkN2dXHK9Jz0uuXF1ozMzMbEaoFktadUguzIai1+aDWfGP1LNKsXgbtbI+I35c+gMeA1wKT02uznS4iBnwsW7aMWbNmscsuuzBr1iyWLVs2aH4HEbP8qt3+e4Wk2en5S8mGI/lr4LuSPrYT6mdWs66uLjo6OjjvvPN46qmnOO+88+jo6KCry7MPmBWp2u2/B0RE/1hWf0U2R8h7gDfg23+tyXR2drJ06VLmz5/PuHHjmD9/PkuXLqWzs7PRVTMb1aoFkq0lz98O/ASeHXn3maIqZTYcvb29zJs3b4dl8+bNo7e3t0E1MhsbqgWSeyUtlvTnZH0jPwWQtDswrujKmQ1Fa2srq1at2mHZqlWraG31PSNmRaoWSNqBWcCJwDER8XBa/kbgWwXWy2zIOjo6aG9vp7u7m61bt9Ld3U17ezsdHR2NrprZqDbo7b9pjpAPV1jeDXQ/fw2zxlm0aBHXX389RxxxBFu2bGG33XbjQx/6EIsWLWp01cxGtUEDiaTlg6VHxML6Vsds+Lq6urjyyitZsWIF8+bNY9WqVbS3t/OmN73JwcSsQIPORyJpI3Av0AX8krLxtSLi2kJrVyeej2RsmD17Nueddx7z589/dll3dzeLFy/m9ts9kWYeHiJlbKp1PpJqgaQFOBxYBBwEXAl0RcTaelV0Z3AgGRtaWlp46qmnGDfuuftAtm7dyoQJE9i+fXsDazbyOZCMTXWZ2CoitkfETyPiBLIO9nXASkmL61TPMUFSXR9Wme/aMmuMqjMkStpN0l8A3wP+Fvg68IOiKzaaVBuio3Sojlrz2fP5ri2zxqjW2f5tYDawAjir5FfuZk2nv0N98eLF9Pb20traSmdnpzvazQpWrY/kGeDx9LI0o4CIiD0HLVxaAHwNaAH+PSK+WJZ+CvBBYBuwEfjr/sEgJZ0AfCZl/XxEfDstfx1wEbA72S/t/y6qnKaPlD4St0Nbs/J3c2yqVx/JLhHxwvTYs+TxwhqCSAtwPnAEMBNYJGlmWbZbgbaIOAi4HDg7rTsJOINsTK+5wBmS+qdV+1fgJGBGeiyotpFmZlacqn0kOcwF1kXE+oh4GrgYOLI0Q0R0R8QT6eWNwNT0/J1kA0RuiojNwNXAgjQC8Z4RcUO6CvkOcFSB22BmZlUUGUj2I/sNSr++tGwg7WR9MYOtu196XrVMSSdJ6pHUs3HjxiFW3czMalVkIKl0n2rFRlZJxwNtwDlV1q25zIi4ICLaIqJtypQpNVTXzMyGo8hA0gfsX/J6KrChPJOkw4AOYGFEbKmybh/PNX8NWKaZme08RQaSm4AZkg6QNB44Fthh7C5JBwPfJAsiD5YkXQW8Q9LE1Mn+DuCqiLgfeFTSG5X9Mu8DwI8K3AYzM6ti0N+R5BER2ySdTBYUWoALI2KtpCVAT0QsJ2vK2gO4LP1i+56IWBgRmyR9jiwYASyJiE3p+Ud47vbfFTzXr2Jmw1DraAm15PMtwmPToL8jGS38OxIzs6Gry+9IzMzMqnEgMTOzXBxIzMwsFwcSMzPLxYHEzMxycSAxM7NcHEjMzCwXBxIzM8vFgcTMzHJxIDEzs1wcSMzMLJfCBm0cKyZNmsTmzZvrVl6tA+hVM3HiRDZt2lQ9o5lZTg4kOW3evLkpB1qsV0AyM6vGTVtmZpaLA4mZmeXiQGJmZrk4kJiZWS4OJGZmlosDiZmZ5eJAYmZmuRQaSCQtkHSnpHWSTquQ/lZJt0jaJunosrQvSbo9PY4pWX6RpN9JWp0ec4rcBjMzG1xhP0iU1AKcDxwO9AE3SVoeEXeUZLsHOBH4ZNm67wJeC8wBdgOulbQiIh5JWT4VEZcXVXczM6tdkVckc4F1EbE+Ip4GLgaOLM0QEXdHxBrgmbJ1ZwLXRsS2iHgcuA1YUGBdzcxsmIoMJPsB95a87kvLanEbcISkF0iaDMwH9i9J75S0RtK5knarVICkkyT1SOrZuHHjcOpvZmY1KHKsrUqDPdU0KFVE/EzS64HrgY3ADcC2lHw68AdgPHABcCqwpEIZF6R02traChsMK87YE87cq6jihy3O2LPRVTCzMaLIQNLHjlcRU4ENta4cEZ1AJ4CkZcBdafn9KcsWSd+irH9lZ9NZjzTtoI1xZqNrYWZjQZFNWzcBMyQdIGk8cCywvJYVJbVI2ic9Pwg4CPhZev3S9FfAUcDtBdTdzMxqVNgVSURsk3QycBXQAlwYEWslLQF6ImJ5ar76ITAReI+ksyJiFjAOuC4Nhf4IcHxE9DdtfV/SFLKms9XAh4vaBjMzq07N2CxTb21tbdHT01NI2ZKat2mrCetlZiOHpJsjoq1aPv+y3czMcnEgMTOzXBxIzMwsFwcSMzPLxYHEzMxycSAxM7NcHEjMzCwXBxIzM8vFgcTMzHJxIDEzs1wcSMzMLBcHEjMzy8WBxMzMcilyYqsxIw1331QmTpzY6CqY2RjhQJJTPYdq99DvZjYSuWnLzMxycSAxM7NcHEjMzCwXBxIzM8vFgcTMzHIpNJBIWiDpTknrJJ1WIf2tkm6RtE3S0WVpX5J0e3ocU7L8AEm/lHSXpEskjS9yG8zMbHCFBRJJLcD5wBHATGCRpJll2e4BTgSWla37LuC1wBzgDcCnJO2Zkr8EnBsRM4DNQHtR22BmZtUVeUUyF1gXEesj4mngYuDI0gwRcXdErAGeKVt3JnBtRGyLiMeB24AFyn75dyhwecr3beCoArfBzMyqKDKQ7AfcW/K6Ly2rxW3AEZJeIGkyMB/YH9gHeDgitlUrU9JJknok9WzcuHFYG2BmZtUVGUgqjRtS08+2I+JnwE+A64Eu4AZg21DKjIgLIqItItqmTJlSW43NzGzIigwkfWRXEf2mAhtqXTkiOiNiTkQcThZA7gIeAvaW1D+0y5DKNDOz+isykNwEzEh3WY0HjgWW17KipBZJ+6TnBwEHAT+LbCCqbqD/Dq8TgB/VveZmZlazwgJJ6sc4GbgK6AUujYi1kpZIWggg6fWS+oC/BL4paW1afRxwnaQ7gAuA40v6RU4FTpG0jqzPZGlR22BmZtVpLIw229bWFj09PY2uRlUe/dfMmomkmyOirVo+/7LdzMxycSAxM7NcHEjMzCwXBxIzM8vFgcTMzHJxIDEzs1wcSMzMLBcHEjMzy8WBxMzMcnEgMTOzXBxIzMwsFwcSMzPLxYHEzMxycSAxM7NcHEjMzCwXBxIzM8vFgcTMzHJxIDEzs1wcSMzMLBcHEjMzy6XQQCJpgaQ7Ja2TdFqF9LdKukXSNklHl6WdLWmtpF5JX5ektHxlKnN1eryoyG0wM7PB7VpUwZJagPOBw4E+4CZJyyPijpJs9wAnAp8sW/dNwJuBg9KiVcAhwMr0+riI6Cmq7vWWYmDd8kZEnuqYmdVVYYEEmAusi4j1AJIuBo4Eng0kEXF3SnumbN0AJgDjAQHjgAcKrGuhfOA3s9GsyKat/YB7S173pWVVRcQNQDdwf3pcFRG9JVm+lZq1/kFDOd03M7O6KzKQVDrA13RqLulAoBWYShZ8DpX01pR8XES8GnhLerx/gDJOktQjqWfjxo1DrryZmdWmyEDSB+xf8noqsKHGdf8cuDEiHouIx4AVwBsBIuK+9PdRYBlZE9rzRMQFEdEWEW1TpkwZ5iaYmVk1RQaSm4AZkg6QNB44Flhe47r3AIdI2lXSOLKO9t70ejJAWv5u4PYC6m5mZjUqLJBExDbgZOAqoBe4NCLWSloiaSGApNdL6gP+EvimpLVp9cuB/wF+DdwG3BYRPwZ2A66StAZYDdwH/FtR22BmZtVpLNxR1NbWFj09I+ZuYTOzpiDp5ohoq5bPv2w3M7NcHEjMzCyXMdG0JWkj8PtG16MGk4GHGl3c5qfYAAAGvElEQVSJUcL7sr68P+trpOzPaRFR9bbXMRFIRgpJPbW0R1p13pf15f1ZX6Ntf7ppy8zMcnEgMTOzXBxImssFja7AKOJ9WV/en/U1qvan+0jMzCwXX5GYmVkuDiRmZpaLA0kTkHShpAcleQDKnCTtL6k7TdG8VtLfNbpOI5mkCZJ+Jem2tD/PanSdRjpJLZJulXRFo+tSLw4kzeEiYEGjKzFKbAM+ERGtZFMP/K2kmQ2u00i2BTg0Il4DzAEWSHpjg+s00v0d2UC2o4YDSROIiF8Amxpdj9EgIu6PiFvS80fJ/mFrmpnTni8yj6WX49LDd+gMk6SpwLuAf290XerJgcRGLUnTgYOBXza2JiNbaopZDTwIXB0R3p/D91Xg74FnGl2RenIgsVFJ0h7AfwAfi4hHGl2fkSwitkfEHLJZTudKmt3oOo1Ekt4NPBgRNze6LvXmQGKjTpo98z+A70fEDxpdn9EiIh4GVuL+vOF6M7BQ0t3AxcChkr7X2CrVhwOJjSqSBCwFeiPinxpdn5FO0hRJe6fnuwOHAb9pbK1Gpog4PSKmRsR0sqnHfx4Rxze4WnXhQNIEJHUBNwB/KqlPUnuj6zSCvRl4P9nZ3ur0+LNGV2oEeynQnaa3vomsj2TU3LZq9eEhUszMLBdfkZiZWS4OJGZmlosDiZmZ5eJAYmZmuTiQmJlZLg4k1nCStqfbdG+TdIukN6Xl+0q6PD2fM9BtvJLeJul/04iqvZLOGGY9rk9/p0t6X8nyNklfH06ZA7zP6ZKOq7D8KElrJP1G0q8lHVVDWQPulyHU52OSXjBA2kpJPSWv2yStHGL5KyW15amjNTcHEmsGT0bEnDTC7OnAPwJExIaIODrlmQMMdsC8LiIOBtqA4yW9bqiViIg3pafTgfeVLO+JiI8OtbxBvAP4WekCSa8BvgwcGRGvAhYCX5Z0UJWyqu2XWnwMqBhIkhdJOiLne9go5kBizWZPYDM8e2Vwu6TxwBLgmHTlcsxAK0fE48DNwCvSXBrfSmf3t0qan8qdlebYWJ2uAGak5f2j3H4ReEtK/3i64rki5Zkk6T/Tejf2H+glnZnmlVkpab2kioFH0p7A+IjYWJb0SeALEfG7tB2/Iwuon0rrPXtWL2mypLsr7ZdUj+9K+rmkuyR9KK3z7Dak1/8s6cRUz33JfnTYPcBuPQf4TIVtGWj/7i7p4rSPLgF2L1nnHZJuSFeel6Ux0WyEcyCxZrB7OhD+hmx47c+VJkbE08BngUvSlcslAxUkaR+yeUjWAn+b1n81sAj4tqQJwIeBr6WBCNuAvrJiTiO7wpkTEeeWpZ0F3BoRBwGfBr5TkvYq4J3AXOCMNOZXucOAayosn0UWAEv1pOUVDbJfDiIbqvz/AJ+VtO8gZXwd2ADMj4j5A2S7AdjSHyhKDLR/PwI8kfZRJ/A6yAIgWUA6LCJem7bvlIHqZiOHA4k1g/6mrVeRDQj4nTRm1lC8RdKtZE1GX4yItcA84LsAEfEb4PfAK8kOjJ+WdCowLSKeHML7lJb5c2AfSXultCsjYktEPEQ25PqLK6y/AFhRYbl4/jwflZbV4kcR8WSqRzdZYMvr8zz/qmSg/ftW4Htp+RpgTcr/RmAm8N/KhqU/AZhWh7pZgzmQWFOJiBuAycCUIa56XUQcHBGvi4hvpGUVg1FELCPrg3gSuErSoUN4n0pl9h/st5Qs2w7sWiHvXOBXFZavJbs6KvVa4I70fBvP/b9OqFLH8uATZevXUsaOBWRBcwJZMOg3WLCvFABFNlbXnPSYGREeV24UcCCxpiLpVUAL8MeypEeBFw6xuF8Ax6VyXwm8DLhT0suB9alZZzlZU1Ct71Va5tuAh2qd70TSLOA3EbG9QvKXgdOVTcbVPynXp4GvpPS7SU1EwNEl61Wq65Gp/2If4G1kgy3+Hpgpabd0BfX2KmVU0kk2KVO/ivu3bPlsntu/NwJvlnRgSntBWs9GOAcSawb9fSSrgUuAEyocbLvJDoSDdraX+RegRdKvU7knRsQW4Bjg9vR+r2LHfg7ImmK2Kbsd+eNlaWcCbcpGw/0iWfNMrY4AflopISJWA6cCP059RT8G/j4thyzQfETZLcqTS1attF9+BVxJduD+XLr77V7g0rRt3wduLSnjAmDFIJ3t/XX8CVB6k8BA+/dfgT3SPvr7VB/SDQYnAl0p7Uay/W8jnEf/NdtJJF0NfCAi7i/wPc4EHouILxf1HmblKrXhmlkBIuLwRtfBrAi+IjEzs1zcR2JmZrk4kJiZWS4OJGZmlosDiZmZ5eJAYmZmufx/vBDBwiYdVWcAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "NUM_OF_TESTS = 50\n",
    "NUM_OF_PLAINTEXTS = 8000\n",
    "\n",
    "positions = [[], [], [], []]\n",
    "for t in range(NUM_OF_TESTS):\n",
    "    key = get_random_bit_lists(5, 16)\n",
    "    cipher = BasicSPNCipher(key)\n",
    "    plaintexts = get_random_bit_lists(NUM_OF_PLAINTEXTS, 16)\n",
    "    ciphertexts = [cipher.encrypt(p, rounds=3)[:4] for p in plaintexts]\n",
    "    plaintexts = array(plaintexts)\n",
    "    ciphertexts = array(ciphertexts)\n",
    "    \n",
    "    model = get_model()\n",
    "    model.fit(plaintexts, ciphertexts,\n",
    "              epochs=EPOCHS, batch_size=BATCH_SIZE,\n",
    "              verbose=0)\n",
    "\n",
    "    preds = model.predict(plaintexts)\n",
    "    mses = mean_squared_error(ciphertexts, preds, multioutput='raw_values')\n",
    "    for i in range(4):\n",
    "        positions[i].append(mses[i])     \n",
    "    K.clear_session()\n",
    "    \n",
    "plt.boxplot([positions[0], positions[1], positions[2], positions[3]])\n",
    "plt.title('Performances for Individual Bit Positions')\n",
    "plt.xlabel('Bit Position / Output Node')\n",
    "plt.ylabel('MSE')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References\n",
    "[AW04]\n",
    "A. M. B. Albassal and A.-M. A. Wahdan. Neural network based cryptanalysis of a Feistel type block cipher. In                 *International Conference on Electrical, Electronic and Computer Engineering, 2004. ICEEC '04*, pages 231–237, 2004.\n",
    "\n",
    "[Hey02]\n",
    "H. M. Heys. A tutorial on linear and differential cryptanalysis. *Cryptologia*, 26(3):189–221, 2002.\n",
    "\n",
    "[Wab19]\n",
    "J. Wabbersen. Cryptanalysis of Block Ciphers Using Feedforward Neural Networks. [http://www.stud.informatik.uni-goettingen.de/~jan.wabbersen/Cryptanalysis_of_Block_Ciphers_Using_Feedforward_Neural_Networks.pdf](http://www.stud.informatik.uni-goettingen.de/~jan.wabbersen/Cryptanalysis_of_Block_Ciphers_Using_Feedforward_Neural_Networks.pdf), 2019."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
