# Cryptanalysis of an SPN Toy Cipher Using FFNNs

The notebook Cryptanalysis.ipynb is a small wrap-up of experimental tests made during my master's thesis.
It demonstrates different key-recovery attacks on block ciphers by using a well-known SPN toy cipher.


## Requirements

Besides Jupyter Notebook and Python, you need the following packages:

* matplotlib
* numpy
* keras (with an underlying library, such as TensorFlow)
* sklearn


## Run Instructions

Download the file Cryptanalysis.ipynb and run it by using Jupyter Notebook.


## Authors

* **Jan Wabbersen** - [ja.wa](https://gitlab.com/ja.wa)


## License

See the [license](license.txt) file.